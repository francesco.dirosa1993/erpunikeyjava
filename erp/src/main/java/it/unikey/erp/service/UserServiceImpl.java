package it.unikey.erp.service;

import it.unikey.erp.DTO.LoginDTO;
import it.unikey.erp.DTO.RegisterDTO;
import it.unikey.erp.DTO.UserDTO;
import it.unikey.erp.entity.UserEntity;
import it.unikey.erp.interfaces.UserService;
import it.unikey.erp.mapper.UserMapper;
import it.unikey.erp.repository.RoleRepository;
import it.unikey.erp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserServiceImpl implements UserService<UserDTO, LoginDTO, RegisterDTO> {
    private UserRepository userRepository;
    private UserMapper userMapper;
    private RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserDTO getByUsernameAndPassword(LoginDTO loginDTO) {
        UserEntity userEntity = userMapper.fromLoginDTOToUserEntity(loginDTO);
        UserEntity loggedEntity = userRepository.findByUsernameAndPassword(userEntity.getUsername(),userEntity.getPassword())
                .orElseThrow(() -> new RuntimeException("User not found"));
        return userMapper.fromUserEntityToUserDTO(loggedEntity);
    }

    @Override
    public UserDTO insert(RegisterDTO registerDTO) {
        return null;
    }

    @Override
    public Set<UserDTO> getAll() {
        return null;
    }

    @Override
    public UserDTO getById(int id) {
        return null;
    }

    @Override
    public UserDTO update(UserDTO userDTO) {
        return null;
    }

    @Override
    public void delete(int id) {

    }
}
