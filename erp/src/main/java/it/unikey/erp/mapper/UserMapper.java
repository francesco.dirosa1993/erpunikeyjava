package it.unikey.erp.mapper;

import it.unikey.erp.DTO.LoginDTO;
import it.unikey.erp.DTO.RegisterDTO;
import it.unikey.erp.DTO.UserDTO;
import it.unikey.erp.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface UserMapper {
    LoginDTO fromUserEntityToLoginDTO(UserEntity userEntity);
    RegisterDTO fromUserEntityToRegisterDTO(UserEntity userEntity);
    UserDTO fromUserEntityToUserDTO(UserEntity userEntity);
    Set<UserDTO> fromUserEntitySetToUserDTOSet(Set<UserEntity> userEntitySet);
    UserEntity fromLoginDTOToUserEntity(LoginDTO loginDTO);
    UserEntity fromRegisterDTOToUserEntity(RegisterDTO registerDTO);
}
