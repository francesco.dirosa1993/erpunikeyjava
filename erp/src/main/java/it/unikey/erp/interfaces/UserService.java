package it.unikey.erp.interfaces;

public interface UserService<U,L,R> extends MyCRUD<U>{
    U getByUsernameAndPassword(L l);
    U insert(R r);
}
