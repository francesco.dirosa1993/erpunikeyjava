package it.unikey.erp.interfaces;

import java.util.Set;

public interface RoleService<T> {
    Set<T> getAll();
    T getById(T t);
}
