package it.unikey.erp.interfaces;

import java.util.Set;

public interface MyCRUD<T> {
    Set<T> getAll();
    T getById(int id);
    T update(T t);
    void delete(int id);

}
